<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/auth.css')}}" />
    <title>Instagram</title>
</head>
<body>
    <main class="flex align-items-center justify-content-center">
        <section id="mobile" class="flex">
        </section>
        <section id="auth" class="flex direction-column">
            <div class="panel login flex direction-column">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h1 title="Instagram" class="flex justify-content-center">
                    <img src="{{asset('img/instagram-logo.png')}}" alt="Instagram logo" title="Instagram logo" />
                </h1>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <label for="email" class="sr-only">E-mail</label>
                    <input name="email" value="{{old('email')}}" placeholder="Masukkan e-mail" />
                    @error('email')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <label for="user_name" class="sr-only">User Name</label>
                    <input name="user_name" value="{{old('user_name')}}" placeholder="Masukkan user_name" />
                    @error('user_name')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <label for="password" class="sr-only">Password</label>
                    <input name="password" type="password" placeholder="Masukkan password" />
                    @error('password')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <label for="password_confirm" id="password-confirm" class="sr-only">Konfirmasi Password</label>
                    <input name="password_confirmation" type="password" placeholder="Masukkan ulang password" />
                    <button type="submit">Buat Akun</button>
                </form>
                <div class="flex separator align-items-center">
                    <span></span>
                    <div class="or">Atau</div>
                    <span></span>
                </div>
                <div class="login-with-fb flex direction-column align-items-center">
                    <div>
                        <img />
                        <a>Masuk dengan Facebook</a>
                    </div>
                </div>
            </div>
            <div class="panel register flex justify-content-center">
                <span>Sudah memiliki akun?</span>
                &nbsp;
                <a href="{{url('login')}}">Masuk</a>
            </div>
            <div class="app-download flex direction-column align-items-center">
                <div class="flex justify-content-center">
                    <img src="{{asset('img/apple-button.png')}}"      alt="Imagem com a logo da Apple Store" title="Imagem com a logo da Apple Store" />
                    <img src="{{asset('img/googleplay-button.png')}}" alt="Imagem com a logo da Google Play" title="Imagem com a logo da Google Play" />
                </div>
            </div>
        </section>
    </main>
</body>
</html>